var APP_DATA = {
  "scenes": [
    {
      "id": "0-sa-entradeta-enamorada",
      "name": "Sa entradeta enamorada",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -2.974558002135705,
        "pitch": -0.023479858477474025,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.685746705007702,
          "pitch": -0.6382961887207141,
          "rotation": 0,
          "target": "3-shabitaci-a-mija-altura"
        },
        {
          "yaw": 0.1234768055756259,
          "pitch": 0.12461130023891975,
          "rotation": 0,
          "target": "1-es-menjador"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "1-es-menjador",
      "name": "Es menjador",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -3.1162214244701083,
        "pitch": 0.26833615832418545,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 3.054533509702532,
          "pitch": 0.07343517519970888,
          "rotation": 0,
          "target": "0-sa-entradeta-enamorada"
        },
        {
          "yaw": 0.5572575049900266,
          "pitch": 0.17653106799330232,
          "rotation": 0,
          "target": "2-es-jard"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "2-es-jard",
      "name": "Es jardí",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -0.07300153148453603,
        "pitch": -0.02609296223912594,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.29755189129298465,
          "pitch": 0.13854468471771497,
          "rotation": 0,
          "target": "1-es-menjador"
        },
        {
          "yaw": -0.14308172675461783,
          "pitch": -0.5782368389990431,
          "rotation": 0,
          "target": "5-ses-voltes"
        },
        {
          "yaw": -0.01609610985517307,
          "pitch": -0.33131353939550223,
          "rotation": 0,
          "target": "4-es-segon-piset"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "3-shabitaci-a-mija-altura",
      "name": "S'habitació a mija altura",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": 3.0165557581559623,
          "pitch": 0.25829264943058305,
          "rotation": 0,
          "target": "4-es-segon-piset"
        },
        {
          "yaw": 0.83618520549512,
          "pitch": 0.25400966762558497,
          "rotation": 0,
          "target": "0-sa-entradeta-enamorada"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "4-es-segon-piset",
      "name": "Es segon piset",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -3.113186857622619,
        "pitch": 0.12170523436188141,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -3.019173591201646,
          "pitch": 0.17075533317729708,
          "rotation": 0,
          "target": "3-shabitaci-a-mija-altura"
        },
        {
          "yaw": 2.8829092403893863,
          "pitch": 0.0467532293154207,
          "rotation": 0,
          "target": "6-shabitacio-dets-alots"
        },
        {
          "yaw": 0.15155752226800168,
          "pitch": 0.22400292589039594,
          "rotation": 0,
          "target": "2-es-jard"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "5-ses-voltes",
      "name": "Ses Voltes",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -3.0734694667545988,
        "pitch": -0.04226261290729738,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.94681083312199,
          "pitch": 0.36553567399729125,
          "rotation": 0,
          "target": "2-es-jard"
        },
        {
          "yaw": 0.051348770655542,
          "pitch": 0.31214394760975495,
          "rotation": 0,
          "target": "7-es-despatx"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "6-shabitacio-dets-alots",
      "name": "S'habitacio dets alots",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": -3.028820459169461,
        "pitch": 0.06122593215587102,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -0.2927059021276808,
          "pitch": -0.1570511138057462,
          "rotation": 0,
          "target": "7-es-despatx"
        },
        {
          "yaw": 0.8514870353755324,
          "pitch": 0.026076918746563038,
          "rotation": 0,
          "target": "3-shabitaci-a-mija-altura"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "7-es-despatx",
      "name": "Es despatx",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "yaw": 3.047342484500179,
        "pitch": 0.01143171839891366,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [
        {
          "yaw": -2.6504636836970956,
          "pitch": 0.19579997084012213,
          "rotation": 0,
          "target": "5-ses-voltes"
        },
        {
          "yaw": -0.34662337484854433,
          "pitch": 0.08853914500648585,
          "rotation": 0,
          "target": "6-shabitacio-dets-alots"
        }
      ],
      "infoHotspots": []
    },
    {
      "id": "8-sa-entradeta",
      "name": "Sa entradeta",
      "levels": [
        {
          "tileSize": 256,
          "size": 256,
          "fallbackOnly": true
        },
        {
          "tileSize": 512,
          "size": 512
        },
        {
          "tileSize": 512,
          "size": 1024
        },
        {
          "tileSize": 512,
          "size": 2048
        }
      ],
      "faceSize": 1520,
      "initialViewParameters": {
        "pitch": 0,
        "yaw": 0,
        "fov": 1.5707963267948966
      },
      "linkHotspots": [],
      "infoHotspots": []
    }
  ],
  "name": "Sa caseta de Sant Feliu",
  "settings": {
    "mouseViewMode": "drag",
    "autorotateEnabled": true,
    "fullscreenButton": false,
    "viewControlButtons": false
  }
};
